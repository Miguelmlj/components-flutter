import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  
  final opciones = ['uno','dos','tres','cuatro','cinco'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('componentes Temp'),
      ),
      body: ListView(
        // children: _crearItems()
        children: _crearItemsCorta()
      ),
    );
  }

  /* 
  List<Widget> _crearItems(){

    List<Widget> lista = new List<Widget>();

    for (String opt in opciones) {
      
      final tempWidget = ListTile(
        title: Text(opt),
      );

      // lista.add(tempWidget);
      // lista.add(Divider());
      //it is the same function above and below
      lista..add(tempWidget)
           ..add(Divider());

    }

    return lista;
  }*/

  List<Widget> _crearItemsCorta(){

    return opciones.map((item){

      return Column(
        children: [
          ListTile(
            title: Text(item + '!'),
            subtitle: Text('anything'),
            leading: Icon(Icons.access_alarm),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: (){},
          ),
          Divider()
        ],
      );
    }).toList();

    
  }

  

}